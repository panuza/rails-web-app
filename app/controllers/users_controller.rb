class UsersController < ApplicationController
	before_action :set_user, only: [:edit, :update, :show]
    before_action :require_same_user, only: [:edit, :update]
	def index
		@users = User.paginate(page: params[:page], per_page: 3)
	end
	
	def new
		@user = User.new

	end

	def create
		@user = User.new(user_params)
		if @user.save
			UserMailer.registration_confirmation(@user).deliver
			# session[:user_id] = @user.id
		 	flash[:success] = "please confirm your email #{@user.username}"
		 	# redirect_to user_path(@user)
		 	redirect_to root_path
		else
		 	render 'new'
		end
	end

	def confirm_email
	  user = User.find_by_confirm_token(params[:id])
	  if user
	    user.email_activate
			session[:user_id] = user.id
	    flash[:success] = "Welcome to the Sample App! Your email has been confirmed."
	    redirect_to root_url
	  else
	    flash[:error] = "Sorry. User does not exist"
	    redirect_to root_url
	  end
	end

	def edit
	end

	def update
		if @user.update(user_params)
		  flash[:success] = "account successfully updated"
		  redirect_to users_path
		else
		 	render 'edit'
		end
	end
	 def show
	 	@user_articles = @user.articles.paginate(page: params[:page], per_page: 3)
	 end

	private
		def user_params
			params.require(:user).permit(:username, :email, :password)
		end

		def set_user
			@user = User.find(params[:id])
		end

		def require_same_user
			if current_user != @user
				flash[:danger] = "You can only edit your own account"
				redirect_to root_path
			end
		end

end