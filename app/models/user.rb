class User < ActiveRecord::Base
	has_many :articles
	before_save { self.email = email.downcase }
	before_validation :confirmation_token, :on => :create

	validates :username, presence: true, 
	           uniqueness: {case_sensitive: false},
	           length: {minimum: 3, maximum:20}
	VALID_EMAIL_REGEX= 	
    /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
	validates :email, presence: true, 
			   uniqueness: {case_sensitive: false},
			   length: {maximum: 105},
			   format: {with: VALID_EMAIL_REGEX}
	has_secure_password

	def email_activate
    self.email_confirmed = true
    self.confirm_token = nil
    save!(:validate => false)
  end

	private
	
	def confirmation_token
    if self.confirm_token.blank?
        self.confirm_token = SecureRandom.urlsafe_base64.to_s
    end
 	end

end